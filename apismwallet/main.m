//
//  main.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 02/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
