//
//  constant.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 08/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#ifndef constant_h
#define constant_h

#define AWTextColorBlack() AWColorFromRGB(43, 43, 43)
#define AWTextColorWhite() AWColorFromRGB(255, 255, 255)
#define AWTextColorGray() AWColorFromRGB(153, 153, 153)

#define AWLineColorGray() AWColorFromRGB(226, 226, 226)
#define AWLineColorRed() AWColorFromRGB(210, 64, 55)
#define AWLineColorDarkRed() AWColorFromHexRGB(0x910000)

#define AWBGColorRed() AWColorFromRGB(210, 64, 55)
#define AWBGColorDarkRed() AWColorFromHexRGB(0x910000)
#define AWBGColorWhite() AWColorFromRGB(255, 255, 255)
#define AWBGColorGray() AWColorFromRGB(153, 153, 153)
#define AWBGColorBlueGray() AWColorFromRGB(247, 248, 252)


#endif /* constant_h */
