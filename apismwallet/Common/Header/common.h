//
//  common.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 02/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#ifndef common_h
#define common_h



/**
 디스플레이 길이 가져오기
 */
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
#define SCREEN_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)
#define SCREEN_SCALE [[UIScreen mainScreen] scale]


/**
 기기 해상도 정의
 */
#define kIsiPhoneResolution35 ([UIScreen mainScreen].bounds.size.height == 480.0f ? YES : NO)
#define kIsiPhoneResolution40 ([UIScreen mainScreen].bounds.size.height == 568.0f ? YES : NO)
#define kIsiPhoneResolution47 ([UIScreen mainScreen].bounds.size.height == 667.0f ? YES : NO)
#define kIsiPhoneResolution55 ([UIScreen mainScreen].bounds.size.height == 736.0f ? YES : NO)



/* ========================================================================================================
 * NSLog
 * ========================================================================================================
 * - 디버그 모드 일 경우, 아래와 같은 방식으로 로그를 출력한다.
 *
 *   [파일명 -[클래스명 메소드명](라인번호)] 실제로그
 *   ex) [MainViewController.m -[MainViewController viewDidLoad](27)] LanguageTestText : 언어테스트 (Korean)
 *
 * - 사용법 : 기본 NSLog 사용법과 동일하다.
 *
 *   ex)
 *     NSLog(@"안녕하세요");
 *     NSLog(@"%@",@"안녕하세요.");
 *     NSLog(@"%d년 %d월 %d일", 2018, 1, 9);
 *
 * ======================================================================================================== */
#ifdef DEBUG
#define NSLog( s, ... ) NSLog( @"[%@ %s(%d)] \n%@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define NSLog( s, ... )
#endif



/* ========================================================================================================
 * AWString
 * ========================================================================================================
 * - 언어 선택시, 앱 내에서 설정한 언어를 우선시 하며, 설정한 언어가 없을 경우 로컬언어를 선택한다.
 *
 *   ex)
 *     AWString(@"main_sub_title");
 *
 * ======================================================================================================== */
#define AWString(_key) [NSBundle.mainBundle localizedStringForKey:(_key) value:@"" table:nil]



/* ========================================================================================================
 * AWSize
 * ========================================================================================================
 * - 375x812 사이즈 기준 비율로 사이즈를 변경한다.
 *
 *   ex)
 *
 * ======================================================================================================== */
#define AWWidth(_width) (_width*([[UIScreen mainScreen] bounds].size.width/375.0))
#define AWHeight(_height) (_height*([[UIScreen mainScreen] bounds].size.height/812.0))
#define AWSize(_width, _height) CGSizeMake(AWWidth(_width), AWHeight(_height))
#define AWSizeWidthRatio(_width, _height) CGSizeMake(AWWidth(_width), AWWidth(_height))
#define AWSizeHeightRatio(_width, _height) CGSizeMake(AWHeight(_width), AWHeight(_height))
#define AWScreenHeight ([[UIScreen mainScreen] bounds].size.height)



/* ========================================================================================================
 * AWColor 매크로
 * ========================================================================================================
 * - RGB HEX코드로 UIColor를 생성한다.
 *
 *   ex)
 *     AWColorFromHexRGB(0xff0000);     //red
 *     AWColorFromRGB(255,0,0);         //red
 *
 * ======================================================================================================== */
#define AWColorFromRGBA(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define AWColorFromRGB(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f]
// HexInt타입
#define AWColorFromHexRGBA(_rgb_value, a) \
[UIColor colorWithRed:((float)((_rgb_value & 0xFF0000) >> 16))/255.0 \
green:((float)((_rgb_value & 0x00FF00) >>  8))/255.0 \
blue:((float)((_rgb_value & 0x0000FF) >>  0))/255.0 \
alpha:a]
#define AWColorFromHexRGB(_rgb_value) AWColorFromHexRGBA(_rgb_value, 1.0)



/* ========================================================================================================
 * Font 메소드 매크로
 * ========================================================================================================
 * - StringManager의 메소드를 호출하는 매크로
 *
 *   ex)
 *     UIFont * font = [UIFont fontWithName:@"Arial" size:17];
 *     CGSize fontSize = AWFontSize(@" 안녕하세요 ", font);
 *     float width = fontSize.width;
 *     float height = fontSize.height;
 *
 * ======================================================================================================== */
#define AWFontSize(_ns_string, _ui_font) [[[NSAttributedString alloc] initWithString:_ns_string attributes:[NSDictionary dictionaryWithObjectsAndKeys:_ui_font, NSFontAttributeName, nil]] size]



/* ========================================================================================================
 * Text 변경 매크로
 * ========================================================================================================
 * - UI의 텍스트를 변경해주는 매크로
 *
 * ======================================================================================================== */
#define AWButtonSetTitle(_btn, _title, comment) [_btn setTitle:AWString(_title, nil) forState:UIControlStateNormal]
#define AWLabelSetText(_label, _text, comment) [_label setText:AWString(_text, nil)]
#define AWTextFieldPlaceHolder(_tfield, _placeholder, comment) [_tfield setPlaceholder:AWString(_placeholder, nil)]



/* ========================================================================================================
 * StringManager 메소드 매크로
 * ========================================================================================================
 * - StringManager의 메소드를 호출하는 매크로
 *
 *   ex)
 *     Trim(@" 안녕하세요 ");                //앞 뒤 공백 제거
 *
 * ======================================================================================================== */
#define Trim(_ns_string) [StringManager getStringTrim:_ns_string]



/* ========================================================================================================
 * ShowToastMessage
 * ========================================================================================================
 * - ShowToastMessage : 토스트를 출력하는 매크로
 *
 *   ex)
 *     ShowToastCopyMessage();                // 클립보드에 저장했을 경우
 *
 * ======================================================================================================== */
#define ShowToastMessageSelf(_string) [self makeToast:_string]
#define ShowToastMessage(_string) [self.view makeToast:_string]
#define ShowToastCopyMessageSelf() ShowToastMessageSelf(AWString(@"CopyMessage"))
#define ShowToastCopyMessage() ShowToastMessage(AWString(@"CopyMessage"))

#endif /* common_h */
