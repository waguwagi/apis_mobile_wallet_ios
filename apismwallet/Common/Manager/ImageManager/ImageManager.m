
//
//  ImageManager.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "ImageManager.h"

@implementation ImageManager
// ==============================================================================
//
//      TODO : 싱글톤 패턴 인스턴스 반환
//
// ==============================================================================
+ (StringManager *)getInstance{
    static StringManager * instance = nil;
    if(instance == nil){
        @synchronized(self){
            if(instance == Nil){
                instance = [[StringManager alloc]init];
            }
        }
    }
    return instance;
}

// ==============================================================================
//
//      TODO : 개발 도구
//
// ==============================================================================

// 개발도구 - QRCode to UIImage
+ (UIImage *)createQRCodeForString:(NSString *)qrString{
    return [ImageManager createQRCodeForString:qrString size:CGSizeMake(240, 240)];
}
+ (UIImage *)createQRCodeForString:(NSString *)qrString size:(CGSize)qrSize {
    // Generation of QR code image
    NSData *qrCodeData = [qrString dataUsingEncoding:NSISOLatin1StringEncoding]; // recommended encoding
    CIFilter *qrCodeFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrCodeFilter setValue:qrCodeData forKey:@"inputMessage"];
    [qrCodeFilter setValue:@"M" forKey:@"inputCorrectionLevel"]; //default of L,M,Q & H modes
    
    CIImage *qrCodeImage = qrCodeFilter.outputImage;
    
    CGRect imageSize = CGRectIntegral(qrCodeImage.extent); // generated image size
    CGSize outputSize = qrSize;
    CIImage *imageByTransform = [qrCodeImage imageByApplyingTransform:CGAffineTransformMakeScale(outputSize.width/CGRectGetWidth(imageSize), outputSize.height/CGRectGetHeight(imageSize))];
    
    UIImage *qrCodeImageByTransform = [UIImage imageWithCIImage:imageByTransform];
    return qrCodeImageByTransform;
}
@end

