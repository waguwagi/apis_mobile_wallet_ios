//
//  ImageManager.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageManager : NSObject

// ==============================================================================
//      TODO : 싱글톤 패턴 인스턴스 반환
// ==============================================================================
+ (StringManager *)getInstance;


// ==============================================================================
//      TODO : 개발 도구
// ==============================================================================
+ (UIImage *)createQRCodeForString:(NSString *)qrString ;
+ (UIImage *)createQRCodeForString:(NSString *)qrString size:(CGSize)qrSize;

@end

NS_ASSUME_NONNULL_END
