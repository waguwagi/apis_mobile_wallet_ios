//
//  StringManager.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 04/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "StringManager.h"

@implementation StringManager

// ==============================================================================
//
//      TODO : 싱글톤 패턴 인스턴스 반환
//
// ==============================================================================
+(StringManager *)getInstance{
    static StringManager * instance = nil;
    if(instance == nil){
        @synchronized(self){
            if(instance == Nil){
                instance = [[StringManager alloc]init];
            }
        }
    }
    return instance;
}

// ==============================================================================
//
//      TODO : 개발 도구
//
// ==============================================================================

// 개발도구 - 문자열 앞뒤 공백제거
+(NSString *)getStringTrim:(NSString *)str{
    if(str == nil){
        str = @"";
    }else{
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    return str;
}

// 개발도구 - 문자열 길이에 맞게 다른 글자로 변경
+(NSString *)getPassword:(NSString *)str mark:(NSString *)mark{
    NSMutableString * mutableString = [[NSMutableString alloc]init];
    for(int i=0 ; i<str.length; i++){
        [mutableString appendString:mark];
    }
    return mutableString;
}
@end
