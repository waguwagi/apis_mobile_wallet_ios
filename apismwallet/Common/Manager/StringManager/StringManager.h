//
//  StringManager.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 04/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface StringManager : NSObject

// ==============================================================================
//      TODO : 싱글톤 패턴 인스턴스 반환
// ==============================================================================
+(StringManager *)getInstance;


// ==============================================================================
//      TODO : 개발 도구
// ==============================================================================
+(NSString *)getStringTrim:(NSString *)str;
+(NSString *)getPassword:(NSString *)str mark:(NSString *)mark;
@end

NS_ASSUME_NONNULL_END
