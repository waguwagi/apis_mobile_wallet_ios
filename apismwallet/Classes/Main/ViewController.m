//
//  ViewController.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 02/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "ViewController.h"
#import "LGSideMenuController.h"
#import "PopupViewController.h"

#import "AWMenuItem.h"
#import "AWMenuController.h"
#import "MainSummaryCell.h"
#import "MainButtonListCell.h"
#import "MainWalletListCell.h"

@interface ViewController ()
// header
// detail
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIView *detailViewDim;
@property (weak, nonatomic) IBOutlet UIImageView *detailViewButtonImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailTopConstraint;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 헤더메뉴 초기화
    [self initHeaderMenu];
    
    // 내용 초기화
    [self initTableView];
    
    // 디테일뷰 초기화
    [self initFooterMenu];
}


// ====================================================
// [ Header ]
// ====================================================

- (void)initHeaderMenu {
    
    
    
}


// ====================================================
// [ Right Menu ]
// ====================================================

- (IBAction)clickRightMenu:(id)sender {
    [self showRightMenu];
}


// ====================================================
// [ Body ]
// ====================================================

-(void) initTableView{
    [super initTableView];
    self.tableView.bounces = NO;
}



// ====================================================
// [ Footer ]
// ====================================================

- (void)initFooterMenu{
    
    // 디테일 뷰 그림자
    self.detailView.layer.shadowOpacity = 0.2f;
    self.detailView.layer.shadowOffset = CGSizeZero;
    self.detailView.layer.shadowColor = AWColorFromHexRGB(0x000000).CGColor;
    self.detailView.layer.shadowRadius = 5.0f;
    
    // 디테일 페이지 숨김
    [self closeDetailView];
}

- (IBAction)clickOpenDetailButton:(id)sender {
    if(self.detailViewDim.hidden){
        [self openDetailViewAnimation];
    }else{
        [self closeDetailViewAnimation];
    }
}


// ====================================================
// [ Other Method ]
// ====================================================


- (void)openDetailView{
    self.detailTopConstraint.constant = -250.0f;
    self.detailViewDim.hidden = NO;
    self.detailViewButtonImage.transform = CGAffineTransformRotate(self.detailViewButtonImage.transform, (M_PI / 2) * (-2) );
}
- (void)openDetailViewAnimation{
    [self rightMenuSwipeGestureEnabled:NO];
    
    [UIView transitionWithView:self.detailViewDim duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self openDetailView];
                        [self.view layoutIfNeeded];
                    } completion:NULL];
    
}
- (void)closeDetailView{
    self.detailTopConstraint.constant = 0.0f;
    self.detailViewDim.hidden = YES;
    self.detailViewButtonImage.transform = CGAffineTransformRotate(self.detailViewButtonImage.transform, (M_PI / 2) * (-2) );
}
- (void)closeDetailViewAnimation{
    [self rightMenuSwipeGestureEnabled:YES];
    
    [UIView transitionWithView:self.detailViewDim duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self closeDetailView];
                        [self.view layoutIfNeeded];
                    } completion:NULL];
}

- (IBAction)showPopup:(id)sender {
    PopupViewController *viewController = [[PopupViewController alloc] initWithNibName:@"PopupViewController" bundle:nil];
    
    [self showPopup:viewController completion:^{
        NSLog(@"팝업이 실행되었습니다.");
    }];
}


#pragma mark - UITableView
- (NSArray<NSString *> *)tableViewCellNibNames {
    return @[@"AWBlankCell", @"MainSummaryCell", @"AWBlankCell", @"MainWalletListCell", @"MainButtonListCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch(indexPath.row){
        case 1:
            return AWWidth([MainSummaryCell estimatedRowHeight]);
        case 3:
            return AWWidth([MainWalletListCell estimatedRowHeight]);
        case 4:
            return AWWidth([MainButtonListCell estimatedRowHeight]);
        default:{
            double contentsSumHeight = [MainSummaryCell estimatedRowHeight] + [MainWalletListCell estimatedRowHeight] + [MainButtonListCell estimatedRowHeight];
            double value = 0.0;
            if (@available(iOS 12.0, *)) {
                value = (self.tableView.visibleSize.height - contentsSumHeight)/3;
            }else{
                value = (self.tableView.frame.size.height - AWWidth(contentsSumHeight))/3;
            }
            return AWWidth(value);
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = nil;
    switch (indexPath.row) {
        case 1:{
            MainSummaryCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"MainSummaryCell" forIndexPath:indexPath];
            
            cell = (UITableViewCell * )tempCell;
            break;
        }
        case 3:{
            MainWalletListCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"MainWalletListCell" forIndexPath:indexPath];
            
            cell = (UITableViewCell * )tempCell;
            break;
        }
        case 4:{
            MainButtonListCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"MainButtonListCell" forIndexPath:indexPath];
            
            cell = (UITableViewCell * )tempCell;
            break;
        }
        default:{
            AWBlankCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            
            cell = (UITableViewCell * )tempCell;
        }
    }
    
    cell.selectionStyle = UITableViewCellAccessoryNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
