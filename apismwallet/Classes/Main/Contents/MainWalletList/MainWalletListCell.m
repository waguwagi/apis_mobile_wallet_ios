//
//  MainWalletListCell.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 09/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "MainWalletListCell.h"
#import "MainWalletListItemCell.h"

#define ItemSize() AWSizeWidthRatio(280, 160)

@interface MainWalletListCell () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (strong, nonatomic) NSArray *dataSource;
@property (strong, nonatomic) LGHorizontalLinearFlowLayout *collectionViewLayout;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (readonly, nonatomic) CGFloat pageWidth;
@property (readonly, nonatomic) CGFloat contentOffset;
@property (assign, nonatomic) NSUInteger animationsCount;
@end

@implementation MainWalletListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    
    [self configureDataSource];
    [self configureCollectionView];
}

+ (double)estimatedRowHeight{
    return 200;
}

#pragma mark - Configuration

- (void)configureCollectionView {
    [self.collectionView registerNib:[UINib nibWithNibName:@"MainWalletListItemCell" bundle:nil] forCellWithReuseIdentifier:@"MainWalletListItemCell"];
    
    self.collectionViewLayout = [LGHorizontalLinearFlowLayout layoutConfiguredWithCollectionView:self.collectionView
                                                                                        itemSize:ItemSize()
                                                                              minimumLineSpacing:AWWidth(-30)];
}

- (void)configureDataSource {
    NSMutableArray *datasource = [NSMutableArray new];
    for (NSUInteger i = 0; i < 2; i++) {
        [datasource addObject:[NSString stringWithFormat:@"Page %@", @(i)]];
    }
    self.dataSource = datasource;
}

#pragma mark - Actions

- (void)scrollToPage:(NSUInteger)page animated:(BOOL)animated {
    self.collectionView.userInteractionEnabled = NO;
    self.animationsCount++;
    CGFloat pageOffset = page * self.pageWidth - self.collectionView.contentInset.left;
    [self.collectionView setContentOffset:CGPointMake(pageOffset, 0) animated:animated];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MainWalletListItemCell *cell =
    (MainWalletListItemCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MainWalletListItemCell"
                                                                    forIndexPath:indexPath];
    cell.pageLabel.text = self.dataSource[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView.isDragging || collectionView.isDecelerating || collectionView.isTracking) return;
    
    //NSUInteger selectedPage = indexPath.row;
}

#pragma mark - UICollectionViewDelegate

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    if (--self.animationsCount > 0) return;
    self.collectionView.userInteractionEnabled = YES;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 스크롤 멈췄을 때
}

#pragma mark - Convenience

- (CGFloat)pageWidth {
    return self.collectionViewLayout.itemSize.width + self.collectionViewLayout.minimumLineSpacing;
}

- (CGFloat)contentOffset {
    return self.collectionView.contentOffset.x + self.collectionView.contentInset.left;
}

@end
