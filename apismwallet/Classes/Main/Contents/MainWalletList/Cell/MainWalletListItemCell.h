//
//  MainWalletListItemCell.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 09/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainWalletListItemCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *pageLabel;

@end

NS_ASSUME_NONNULL_END
