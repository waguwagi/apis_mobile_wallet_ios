//
//  MainSummaryCell.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 08/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "MainSummaryCell.h"

@implementation MainSummaryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.titleLabel setText:AWString(@"APIS WALLET")];
    [self.titleLabel setTextColor:AWColorFromHexRGB(0x999999)];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickSecretButton:(id)sender {
    self.secretButton.selected = !self.secretButton.selected;
}

+ (double)estimatedRowHeight{
    return 140;
}
@end
