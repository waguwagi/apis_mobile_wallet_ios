//
//  MainSummaryCell.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 08/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainSummaryCell : BaseView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet AWSecretButton *secretButton;

@end

NS_ASSUME_NONNULL_END
