//
//  MainButtonListCell.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 09/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "MainButtonListCell.h"

@implementation MainButtonListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (double)estimatedRowHeight{
    return 60;
}

@end
