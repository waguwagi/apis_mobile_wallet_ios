//
//  SideMenuViewController.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 10/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SideMenuItemCell.h"

@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.bounces = YES;
}

#pragma mark - UITableView

- (NSArray<NSString *> *)tableViewCellNibNames {
    return @[@"SideMenuItemCell", @"SideMenuItemCell", @"SideMenuItemCell", @"SideMenuItemCell", @"SideMenuItemCell", @"AWBlankCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = nil;
    switch (indexPath.row) {
        case 0:{
            
            SideMenuItemCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuItemCell" forIndexPath:indexPath];
            tempCell.titleLabel.text = AWString(@"Create Wallet");
            [tempCell.iconButton setImage:[UIImage imageNamed:@"ic_createwallet"] forState:UIControlStateNormal];
            cell = (UITableViewCell * )tempCell;
            break;
        }
        case 1:{
            SideMenuItemCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuItemCell" forIndexPath:indexPath];
            tempCell.titleLabel.text = AWString(@"Load Wallet");
            [tempCell.iconButton setImage:[UIImage imageNamed:@"ic_loadwallet"] forState:UIControlStateNormal];
            cell = (UITableViewCell * )tempCell;
            break;
        }
        case 2:{
            
            SideMenuItemCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuItemCell" forIndexPath:indexPath];
            tempCell.titleLabel.text = AWString(@"Transaction");
            [tempCell.iconButton setImage:[UIImage imageNamed:@"ic_transfer"] forState:UIControlStateNormal];
            cell = (UITableViewCell * )tempCell;
            break;
        }
        case 3:{
            
            SideMenuItemCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuItemCell" forIndexPath:indexPath];
            tempCell.titleLabel.text = AWString(@"Address Masking");
            [tempCell.iconButton setImage:[UIImage imageNamed:@"ic_addressmasking"] forState:UIControlStateNormal];
            cell = (UITableViewCell * )tempCell;
            break;
        }
        case 4:{
            
            SideMenuItemCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuItemCell" forIndexPath:indexPath];
            tempCell.titleLabel.text = AWString(@"Language");
            [tempCell.iconButton setImage:[UIImage imageNamed:@"ic_language"] forState:UIControlStateNormal];
            cell = (UITableViewCell * )tempCell;
            break;
        }
        default:{
            AWBlankCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            
            cell = (UITableViewCell * )tempCell;
        }
    }
    
    cell.selectionStyle = UITableViewCellAccessoryNone;
    return cell; 
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            [self toCreateWallet];
            break;
        case 1:
            [self toLoadWallet];
            break;
            
        default:
            break;
    }
}
@end
