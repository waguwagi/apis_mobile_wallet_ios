//
//  SideMenuItemCell.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 10/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SideMenuItemCell : BaseView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *iconButton;

@end

NS_ASSUME_NONNULL_END
