//
//  SideMenuItemCell.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 10/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "SideMenuItemCell.h"

@implementation SideMenuItemCell

- (IBAction)touchDown:(id)sender {
    self.backgroundColor = AWColorFromHexRGB(0xebecf0);
}
- (IBAction)touchUpInside:(id)sender {
    self.backgroundColor = AWColorFromHexRGB(0xffffff);
}

@end
