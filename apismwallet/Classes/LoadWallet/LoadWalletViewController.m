//
//  LoadWalletViewController.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "LoadWalletViewController.h"

#import "PopupViewController.h"

#define kLoadWalletMain         0
#define kLoadKeystoreFileStep1  1
#define kLoadKeystoreFileStep2  2
#define kLoadPrivateKeyStep1    3
#define kLoadPrivateKeyStep2    4

#define kViewTagQRCodeScannerPopup 1000

@interface LoadWalletViewController ()<PopupViewControllerDelegate, AWHeaderDelegate, AWFooterDelegate, AWSelectOnlyOneDelegate, AWScanPrivateKeyDelegate>
{
    int step;
    NSInteger path;
    NSArray<NSString *> * subTitles;
    
    NSString * loadPrivateKeyQRCode;
}
@end

@implementation LoadWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    step = kLoadWalletMain;
    subTitles = @[AWString(@"How would you like to add your wallet?"), AWString(@"Select your Keystore file and enter your password"), AWString(@"Select your Keystore file and enter your password"), AWString(@"Enter your private key"), AWString(@"Wallet name & password")];
    
    self.header.title.text = AWString(@"Load Wallet");
    [self.header setStep:step subTitle:subTitles[step]];
    
    self.header.delegate = self;
    self.footer.delegate = self;
}

// 스텝으로 이동
- (void)setStep:(int)index{
    step = index;
    [self.header setStep:step subTitle:subTitles[step]];
    [UIView transitionWithView: self.tableView duration: 0.2f options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void) {
                        [self.tableView reloadData];
                    } completion: nil];
}


// PrivateKey Scan 팝업 호출
- (void)openPopupTypeQRCodeScan{
    PopupViewController *viewController = [[PopupViewController alloc] initWithNibName:@"PopupViewController" bundle:nil];
    viewController.backgroundDelay = 0.0f;
    viewController.popupNibName = @"QRCodeScan";
    viewController.delegate = self;
    viewController.view.tag = kViewTagQRCodeScannerPopup;
    
    viewController.backgroundDelay = 0.4f;
    [self showPopupAnimation:viewController completion:^{
    }];
}

#pragma mark - PopupViewControllerDelegate
- (void)action:(PopupViewController *)viewController view:(BasePopupView * )view type:(PopupAction)action{
    if(viewController.view.tag == kViewTagQRCodeScannerPopup){
        if(action == PopupActionCancel){
        }else if(action == PopupActionResult){
            loadPrivateKeyQRCode = view.model.privateKey;
        }
        [viewController close];
        [self.tableView reloadData];
    }
}

#pragma mark - AWHeaderDelegate
- (void)backAWHeaderDelegate{
    if(step == kLoadWalletMain){
        [super back];
    }else if(step == kLoadKeystoreFileStep1){
        [self setStep: kLoadWalletMain];
    }else if(step == kLoadKeystoreFileStep2){
        [self setStep: kLoadKeystoreFileStep1];
    }else if(step == kLoadPrivateKeyStep1){
        [self setStep: kLoadWalletMain];
    }else if(step == kLoadPrivateKeyStep2){
        [self setStep: kLoadPrivateKeyStep1];
    }else{
        [self setStep:step];
    }
}

#pragma mark - AWFooterDelegate
- (void)nextAWFooterDelegate{
    if(step == kLoadWalletMain){
        if(path == 0){
            [self setStep: kLoadKeystoreFileStep1];
        }else{
            [self setStep: kLoadPrivateKeyStep1];
        }
    }else if(step == kLoadKeystoreFileStep1){
        [self setStep: kLoadKeystoreFileStep2];
    }else if(step == kLoadKeystoreFileStep2){
        [super back];
    }else if(step == kLoadPrivateKeyStep1){
        [self setStep: kLoadPrivateKeyStep2];
    }else if(step == kLoadPrivateKeyStep2){
        [super back];
    }else{
        [self setStep:step];
    }
}

#pragma mark - AWSelectOnlyOneDelegate
- (void)selected:(AWSelectButton * )button index:(NSInteger)index{
    path = index;
}

#pragma mark - AWScanPrivateKeyDelegate
- (void)openScanPopupAWScanPrivateKeyDelegate{
    [self openPopupTypeQRCodeScan];
}

#pragma mark - UITableView
- (NSArray<NSString *> *)tableViewCellNibNamesAll {
    return @[@"AWBlankCell", @"AWWalletNameField", @"AWWalletConfirmPasswordField", @"AWInfoMessage", @"AWInfoMessageSub2", @"AWInfoMessageSub3", @"AWDownloadKeystoreFile", @"AWPrivateKeyBackup", @"AWSelectOnlyOne", @"AWSelectKeystoreFile", @"AWPasswordField", @"AWScanPrivateKey"];
}
- (NSArray<NSString *> *)tableViewCellNibNames {
    if(step == 0){
        return @[@"AWSelectOnlyOne", @"AWBlankCell"];
    }else if(step == 1){
        return @[@"AWSelectKeystoreFile", @"AWPasswordField", @"AWInfoMessage", @"AWBlankCell"];
    }else if(step == 2){
        return @[@"AWWalletNameField", @"AWBlankCell"];
    }else if(step == 3){
        return @[@"AWScanPrivateKey", @"AWInfoMessage", @"AWInfoMessageSub2", @"AWBlankCell"];
    }else if(step == 4){
        return @[@"AWWalletNameField", @"AWWalletConfirmPasswordField", @"AWInfoMessage", @"AWBlankCell"];
    }else{
        return @[@"AWBlankCell"];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = nil;
    switch (step) {
        case 0: { cell = [self step1TableView:tableView cellForRowAtIndexPath:indexPath]; break; }
        case 1: { cell = [self step2TableView:tableView cellForRowAtIndexPath:indexPath]; break; }
        case 2: { cell = [self step3TableView:tableView cellForRowAtIndexPath:indexPath]; break; }
        case 3: { cell = [self step4TableView:tableView cellForRowAtIndexPath:indexPath]; break; }
        case 4: { cell = [self step5TableView:tableView cellForRowAtIndexPath:indexPath]; break; }
        default: {
            AWBlankCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            tempCell.backgroundColor = AWBGColorBlueGray();
            cell = (UITableViewCell * )tempCell;
            break;
        }
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    return cell;
}

- (UITableViewCell *)step1TableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            AWSelectOnlyOne * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWSelectOnlyOne" forIndexPath:indexPath];
            tempCell.delegate = self;
            return (UITableViewCell * )tempCell;
        }
        default:{
            AWBlankCell * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            tempCell.backgroundColor = AWBGColorBlueGray();
            return (UITableViewCell * )tempCell;
        }
    }
}

- (UITableViewCell *)step2TableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            AWSelectKeystoreFile * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWSelectKeystoreFile" forIndexPath:indexPath];
            
            return (UITableViewCell * )tempCell;
        }
        case 1:{
            AWPasswordField * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWPasswordField" forIndexPath:indexPath];
            
            return (UITableViewCell * )tempCell;
        }
        case 2:{
            AWInfoMessage * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWInfoMessage" forIndexPath:indexPath];
            [tempCell setText:AWString(@"you are responsible for keeping your password safe.\nDo not forget to save this. you cannot restore it.")];
            return (UITableViewCell * )tempCell;
        }
        default:{
            AWBlankCell * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            tempCell.backgroundColor = AWBGColorBlueGray();
            return (UITableViewCell * )tempCell;
        }
    }
}
- (UITableViewCell *)step3TableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            AWWalletNameField * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWWalletNameField" forIndexPath:indexPath];
            return (UITableViewCell * )tempCell;
        }
        default:{
            AWBlankCell * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            tempCell.backgroundColor = AWBGColorBlueGray();
            return (UITableViewCell * )tempCell;
        }
    }
}
- (UITableViewCell *)step4TableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            AWScanPrivateKey * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWScanPrivateKey" forIndexPath:indexPath];
            if(loadPrivateKeyQRCode){
                [tempCell setPrivateKey:loadPrivateKeyQRCode];
            }
            tempCell.delegate = self;
            return (UITableViewCell * )tempCell;
        }
        case 1:{
            AWInfoMessage * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWInfoMessage" forIndexPath:indexPath];
            [tempCell setText:AWString(@"you are responsible for keeping your password safe.\nDo not forget to save this. you cannot restore it.")];
            return (UITableViewCell * )tempCell;
        }
        case 2:{
            AWInfoMessageSub2 * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWInfoMessageSub2" forIndexPath:indexPath];
            [tempCell setText:AWString(@"Use this Keystore file to load your wallet from other devices.")];
            return (UITableViewCell * )tempCell;
        }
        default:{
            AWBlankCell * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            tempCell.backgroundColor = AWBGColorBlueGray();
            return (UITableViewCell * )tempCell;
        }
    }
}

- (UITableViewCell *)step5TableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            AWWalletNameField * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWWalletNameField" forIndexPath:indexPath];
            return (UITableViewCell * )tempCell;
        }
        case 1:{
            AWWalletConfirmPasswordField * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWWalletConfirmPasswordField" forIndexPath:indexPath];
            return (UITableViewCell * )tempCell;
        }
        case 2:{
            AWInfoMessage * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWInfoMessage" forIndexPath:indexPath];
            [tempCell setText:AWString(@"you are responsible for keeping your password safe.\nDo not forget to save this. you cannot restore it.")];
            return (UITableViewCell * )tempCell;
        }
        default:{
            AWBlankCell * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            tempCell.backgroundColor = AWBGColorBlueGray();
            return (UITableViewCell * )tempCell;
        }
    }
}
@end
