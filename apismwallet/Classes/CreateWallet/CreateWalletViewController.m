//
//  CreateWalletViewController.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 11/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "CreateWalletViewController.h"
#import "PopupViewController.h"

#define kViewTagDownloadKeystoreFileCautionPopup 1000
#define kViewTagQRCodeBackupPopup 1001

@interface CreateWalletViewController ()<PopupViewControllerDelegate, AWHeaderDelegate, AWFooterDelegate, AWDownloadKeystoreFileDelegate, AWPrivateKeyBackupDelegate>
{
    int step;
    
    NSString * _walletName;
    NSString * _privateKey;
    NSArray<NSString *> * subTitles;
}
@end

@implementation CreateWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    step = 0;
    subTitles = @[AWString(@"Wallet name & password"), AWString(@"Keystore file"), AWString(@"Keystore file")];
    
    self.header.title.text = AWString(@"Create Wallet");
    self.header.naviUesd = YES;
    [self.header setStep:step subTitle:subTitles[step]];
    
    self.header.delegate = self;
    self.footer.delegate = self;
    
    _walletName = @"Daryl 001";
    _privateKey = @"private key";
    
}

// 이전 스텝으로 이동
- (void)prevStep{
    step--;
    [self.header setStep:step subTitle:subTitles[step]];
    [UIView transitionWithView: self.tableView duration: 0.2f options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void) {
                        [self.tableView reloadData];
                    } completion: nil];
}

// 다음 스텝으로 이동
- (void)nextStep{
    step ++;
    [self.header setStep:step subTitle:subTitles[step]];
    [UIView transitionWithView: self.tableView duration: 0.2f options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void) {
                        [self.tableView reloadData];
                    } completion: nil];
}

// 키스토어 파일 경고 팝업 호출
- (void)openPopupTypeCaution{
    PopupViewController *viewController = [[PopupViewController alloc] initWithNibName:@"PopupViewController" bundle:nil];
    viewController.backgroundDelay = 0.0f;
    viewController.popupNibName = @"PopupCaution";
    viewController.delegate = self;
    viewController.view.tag = kViewTagDownloadKeystoreFileCautionPopup;
    [self showPopup:viewController completion:^{
    }];
}

// 개인키 팝업 호출
- (void)openPopupPrivateKeyBackup{
    PopupViewController *viewController = [[PopupViewController alloc] initWithNibName:@"PopupViewController" bundle:nil];
    
    BasePopupModel * model = [[BasePopupModel alloc]init];
    model.walletName = _walletName;
    model.privateKey = _privateKey;
    
    viewController.popupNibName = @"QRCodeBackup";
    viewController.model = model;
    viewController.delegate = self;
    viewController.view.tag = kViewTagQRCodeBackupPopup;
    
    viewController.backgroundDelay = 0.3f;
    [self showPopupAnimation:viewController completion:^{
    }];
}


#pragma mark - PopupViewControllerDelegate
- (void)action:(PopupViewController *)viewController view:(BasePopupView * )view type:(PopupAction)action{
    if(viewController.view.tag == kViewTagDownloadKeystoreFileCautionPopup){
        [self nextStep];
        [viewController close];
    }
}


#pragma mark - AWHeaderDelegate
- (void)backAWHeaderDelegate{
    if(step == 0){
        [super back];
    }else{
        [self prevStep];
    }
}

#pragma mark - AWFooterDelegate
- (void)nextAWFooterDelegate{
    if(step == 0){
        [self nextStep];
    }
    
    else if(step == 1){
        
        // 다운로드하지 않고 Next 눌렀을 경우 팝업 보임
        [self openPopupTypeCaution];
        return;
    }
    
    else if(step == 2){
        [self back];
    }
}

#pragma mark - AWDownloadKeystoreFileDelegate
- (void)awDownloadKeystoreFileDelegateDownloadKeyStoreFile:(AWDownloadKeystoreFile * )cell{
    
}

#pragma mark - AWPrivateKeyBackupDelegate
- (void)copy:(AWPrivateKeyBackup * )view text:(NSString *)text{
    ShowToastCopyMessage();
}
- (void)showQRCode:(AWPrivateKeyBackup * )view privateKey:(NSString *)key{
    _privateKey = key;
    [self openPopupPrivateKeyBackup];
}


#pragma mark - UITableView
- (NSArray<NSString *> *)tableViewCellNibNamesAll {
    return @[@"AWBlankCell", @"AWWalletNameField", @"AWWalletConfirmPasswordField", @"AWInfoMessage", @"AWInfoMessageSub2", @"AWInfoMessageSub3", @"AWDownloadKeystoreFile", @"AWPrivateKeyBackup"];
}
- (NSArray<NSString *> *)tableViewCellNibNames {
    if(step == 0){
        return @[@"AWWalletNameField", @"AWWalletConfirmPasswordField", @"AWInfoMessage", @"AWBlankCell"];
    }else if(step == 1){
        return @[@"AWDownloadKeystoreFile", @"AWInfoMessage", @"AWInfoMessageSub3", @"AWInfoMessageSub2", @"AWBlankCell"];
    }else{
        return @[@"AWPrivateKeyBackup",@"AWInfoMessage",@"AWInfoMessageSub2", @"AWBlankCell"];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = nil;
    switch (step) {
        case 0: { cell = [self step1TableView:tableView cellForRowAtIndexPath:indexPath]; break; }
        case 1: { cell = [self step2TableView:tableView cellForRowAtIndexPath:indexPath]; break; }
        case 2: { cell = [self step3TableView:tableView cellForRowAtIndexPath:indexPath]; break; }
        default: {
            AWBlankCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            tempCell.backgroundColor = AWBGColorBlueGray();
            cell = (UITableViewCell * )tempCell;
            break;
        }
    }
    cell.selectionStyle = UITableViewCellAccessoryNone;
    return cell;
}

- (UITableViewCell *)step1TableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            AWWalletNameField * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWWalletNameField" forIndexPath:indexPath];
            return (UITableViewCell * )tempCell;
        }
        case 1:{
            AWWalletConfirmPasswordField * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWWalletConfirmPasswordField" forIndexPath:indexPath];
            return (UITableViewCell * )tempCell;
        }
        case 2:{
            AWInfoMessage * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWInfoMessage" forIndexPath:indexPath];
            [tempCell setText:AWString(@"you are responsible for keeping your password safe.\nDo not forget to save this. you cannot restore it.")];
            return (UITableViewCell * )tempCell;
        }
        default:{
            AWBlankCell * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            tempCell.backgroundColor = AWBGColorBlueGray();
            return (UITableViewCell * )tempCell;
        }
    }
}

- (UITableViewCell *)step2TableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{
            AWDownloadKeystoreFile * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWDownloadKeystoreFile" forIndexPath:indexPath];
            tempCell.delegate = self;
            
            return (UITableViewCell * )tempCell;
        }
        case 1:{
            AWInfoMessage * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWInfoMessage" forIndexPath:indexPath];
            [tempCell setText:AWString(@"The private key is unique to your wallet and has no password.")];
            return (UITableViewCell * )tempCell;
        }
        case 2:{
            AWInfoMessageSub3 * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWInfoMessageSub3" forIndexPath:indexPath];
            [tempCell setText:AWString(@"Pay special attention to your private key as anymore who can access to the private key can transfer assets from the wallets without your permission")];
            return (UITableViewCell * )tempCell;
        }
        case 3:{
            AWInfoMessageSub2 * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWInfoMessageSub2" forIndexPath:indexPath];
            [tempCell setText:AWString(@"You can access to your private key from the ‘Backup wallet’ menu later.")];
            return (UITableViewCell * )tempCell;
        }
        default:{
            AWBlankCell * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            tempCell.backgroundColor = AWBGColorBlueGray();
            return (UITableViewCell * )tempCell;
        }
    }
}

- (UITableViewCell *)step3TableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            AWPrivateKeyBackup * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWPrivateKeyBackup" forIndexPath:indexPath];
            tempCell.delegate = self;
            return (UITableViewCell * )tempCell;
        }
        case 1:{
            AWInfoMessage * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWInfoMessage" forIndexPath:indexPath];
            [tempCell setText:AWString(@"This Keystore file contains the encrypted private key and requires the wallet password to access it.")];
            return (UITableViewCell * )tempCell;
        }
        case 2:{
            AWInfoMessageSub2 * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWInfoMessageSub2" forIndexPath:indexPath];
            [tempCell setText:AWString(@"Use this Keystore file to load your wallet from other devices.")];
            return (UITableViewCell * )tempCell;
        }
        default:{
            AWBlankCell * tempCell = [self.tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            tempCell.backgroundColor = AWBGColorBlueGray();
            return (UITableViewCell * )tempCell;
        }
    }
}
@end
