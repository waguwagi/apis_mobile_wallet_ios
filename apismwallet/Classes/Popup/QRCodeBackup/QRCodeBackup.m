//
//  QRCodeBackup.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "QRCodeBackup.h"

@implementation QRCodeBackup

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.containerCopyView.layer.borderColor = AWLineColorGray().CGColor;
    self.containerCopyView.layer.borderWidth = 1.0f;
    
    self.privateKey.textContainer.lineFragmentPadding = 0;
    self.privateKey.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.privateKey.font = [UIFont systemFontOfSize:AWWidth(12) weight:UIFontWeightRegular];
    [self.privateKey setScrollEnabled:NO];
    
}
- (void)setModel:(BasePopupModel *)model{
    self.walletName.text = model.walletName;
    self.privateKey.text = model.privateKey;
    self.qrcode.image = [ImageManager createQRCodeForString:self.privateKey.text size:CGSizeMake(AWWidth(240), AWWidth(240))];
}

- (IBAction)copy:(id)sender {
    [self.textCopyButton setCopyString:self.privateKey.text];
    ShowToastCopyMessageSelf();
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
