//
//  QRCodeBackup.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BasePopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QRCodeBackup : BasePopupView
@property (weak, nonatomic) IBOutlet UIView *containerCopyView;
@property (weak, nonatomic) IBOutlet AWLabelMainTitle *walletName;
@property (weak, nonatomic) IBOutlet UITextView *privateKey;
@property (weak, nonatomic) IBOutlet AWCopyButton *textCopyButton;
@property (weak, nonatomic) IBOutlet UIImageView *qrcode;

@end

NS_ASSUME_NONNULL_END
