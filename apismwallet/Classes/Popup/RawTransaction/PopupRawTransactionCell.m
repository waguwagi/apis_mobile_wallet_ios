//
//  PopupRawTransactionCell.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 07/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "PopupRawTransactionCell.h"

@implementation PopupRawTransactionCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)clickCopyButton:(id)sender {
    NSString * copy = Trim(self.textView.text);
    [self.rawCopyButton setCopyString:copy];
    
    [self.delegate action:self type:PopupActionCopy];
}

@end
