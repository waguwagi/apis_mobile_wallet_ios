//
//  PopupRawTransactionCell.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 07/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BasePopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopupRawTransactionCell : BasePopupView
@property (weak, nonatomic) IBOutlet BaseTextView *textView;
@property (weak, nonatomic) IBOutlet AWCopyButton *rawCopyButton;

@end

NS_ASSUME_NONNULL_END
