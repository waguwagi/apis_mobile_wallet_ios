//
//  PopupViewController.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "PopupViewController.h"

@interface PopupViewController ()<BasePopupViewDelegate>

@end

@implementation PopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self enableTouchDismisKeyboard:YES];
}
- (void) close{
    [super back];
}

#pragma mark BasePopupViewDelegate
- (void)action:(BasePopupView * )view type:(PopupAction)action{
    if(self.delegate){
        [self.delegate action:self view:view type:action];
    }
}

#pragma mark - UITableView
- (NSArray<NSString *> *)tableViewCellNibNames {
    return @[_popupNibName];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        CGFloat topPadding = window.safeAreaInsets.top;
        CGFloat bottomPadding = window.safeAreaInsets.bottom;
        CGFloat safeAreaHeight  = SCREEN_HEIGHT - topPadding - bottomPadding;
        return safeAreaHeight;
    }else{
        return tableView.frame.size.height;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BasePopupView * cell = (BasePopupView * )[tableView dequeueReusableCellWithIdentifier:_popupNibName forIndexPath:indexPath];
    cell.delegate = self;
    cell.model = self.model;
    if(cell){
        cell.selectionStyle = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
