//
//  PopupViewController.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BasePopupViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PopupViewControllerDelegate;

@interface PopupViewController : BasePopupViewController
@property (assign, nonatomic) id<PopupViewControllerDelegate> delegate;
@property (copy, nonatomic) NSString *popupNibName;

- (void) close;

@end

@protocol PopupViewControllerDelegate <NSObject>
- (void)action:(PopupViewController *)viewController view:(BasePopupView * )view type:(PopupAction)action;
@end
NS_ASSUME_NONNULL_END
