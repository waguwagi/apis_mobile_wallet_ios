//
//  PopupSuccessCell.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 07/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BasePopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopupSuccessCell : BasePopupView

@end

NS_ASSUME_NONNULL_END
