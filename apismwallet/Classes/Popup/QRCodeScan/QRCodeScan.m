//
//  QRCodeScan.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 21/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "QRCodeScan.h"
#import <AVFoundation/AVFoundation.h>

@interface QRCodeScan()<AVCaptureMetadataOutputObjectsDelegate>
{
    void(^_completion)(BOOL, NSString *);
    NSMutableArray *_observers;
}
@property (nonatomic) BOOL isReading;
@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@end

@implementation QRCodeScan

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupNotifications];
    
    [self startReading];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
- (IBAction)cancel:(id)sender {
    if(self.delegate){
        [self.delegate action:self type:PopupActionCancel];
    }
}
- (void)dealWithResult:(NSString *)result {
    BasePopupModel * model = [[BasePopupModel alloc]init];
    model.privateKey = result;
    self.model = model;
    if(self.delegate){
        [self.delegate action:self type:PopupActionResult];
    }
}

- (void)startReading {
    if (!_isReading) {
        NSError *error;
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        
        // Get an instance of the AVCaptureDeviceInput class using the previous device object.
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
        
        if (input) {
            // Initialize the captureSession object.
            _captureSession = [[AVCaptureSession alloc] init];
            // Set the input device on the capture session.
            [_captureSession addInput:input];
            
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
            [_captureSession addOutput:captureMetadataOutput];
            
            // Create a new serial dispatch queue.
            dispatch_queue_t dispatchQueue;
            dispatchQueue = dispatch_queue_create("myQueue", NULL);
            [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
            [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
            [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
            [_videoPreviewLayer setFrame:_scanPreView.layer.bounds];
            [_scanPreView.layer addSublayer:_videoPreviewLayer];
            
            
            // Start video capture.
            [_captureSession startRunning];
            _isReading = !_isReading;
        } else {
            // If any error occurs, simply log the description of it and don't continue any more.
            NSLog(@"%@", [error localizedDescription]);
            return;
        }
    }
}

- (void)stopReading {
    if (_isReading) {
        // Stop video capture and make the capture session object nil.
        [_captureSession stopRunning];
        _captureSession = nil;
        
        // Remove the video preview layer from the viewPreview view's layer.
        [_videoPreviewLayer removeFromSuperlayer];
        _isReading = !_isReading;
    }
}

- (void)setupNotifications{
    if (!_observers) {
        _observers = [NSMutableArray array];
    }
    
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    
    __weak QRCodeScan * SELF = self;
    id o;
    
    o = [center addObserverForName:UIApplicationDidEnterBackgroundNotification
                            object:nil
                             queue:nil
                        usingBlock:^(NSNotification *note) {
                            [SELF.imagePicker dismissViewControllerAnimated:NO completion:NULL];
                            [SELF cancel:SELF.closeButton];
                        }];
    [_observers addObject:o];
}


#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            
            void(^block)(void) = ^(void) {
                [self stopReading];
                [self cancel:self->_closeButton];
                if (![metadataObj stringValue] || [[metadataObj stringValue] length] == 0) {
                    if (self->_completion) {
                        self->_completion(NO, nil);
                    }
                    [self dealWithResult:nil];
                } else {
                    if (self->_completion) {
                        self->_completion(YES, [metadataObj stringValue]);
                    }
                    [self dealWithResult:[metadataObj stringValue]];
                }
            };

            if ([NSThread isMainThread]) {
                block();
            } else {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    block();
                });
            }
        }else{
        }
    }
}
@end
