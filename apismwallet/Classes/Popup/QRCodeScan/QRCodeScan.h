//
//  QRCodeScan.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 21/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BasePopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QRCodeScan : BasePopupView
@property (weak, nonatomic) IBOutlet UIView *scanPreView;
@property (weak, nonatomic) IBOutlet AWRedRectButton *closeButton;
@end

NS_ASSUME_NONNULL_END
