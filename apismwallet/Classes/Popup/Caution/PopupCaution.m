//
//  PopupCaution.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "PopupCaution.h"

@implementation PopupCaution

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)next:(id)sender {
    if(self.delegate){
        [self.delegate action:self type:PopupActionNext];
    }
}

@end
