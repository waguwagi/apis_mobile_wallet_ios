//
//  AWMenuItem.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 07/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWMenuItem.h"


@interface AWMenuItem()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UIView *bottomLine;
@property (strong, nonatomic) UIButton *button;

@end
@implementation AWMenuItem


- (void)initData{
}
    
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initData];
        [self initLayout];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
        [self initLayout];
    }
    return self;
}

- (void)initLayout{
    [[self layer] setMasksToBounds:YES];
    
    UIFont * font = [UIFont fontWithName:@"Kefa" size:17];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.titleLabel setFont:font];
    
    self.button = [[UIButton alloc] initWithFrame:CGRectZero];
    [self.button addTarget:self action:NSSelectorFromString(@"clickMenuItem") forControlEvents:UIControlEventTouchUpInside];
    
    self.bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 2, 100, 2)];
    self.bottomLine.backgroundColor = AWColorFromHexRGB(0xd24037);
    
    
    [self addSubview:self.titleLabel];
    [self addSubview:self.button];
    [self addSubview:self.bottomLine];
}

- (void)stateSelected{
    [self.titleLabel setTextColor:AWColorFromHexRGB(0xd24037)];
    [self.bottomLine setHidden:NO];
}

- (void)stateUnSelected{
    [self.titleLabel setTextColor:AWColorFromHexRGB(0xc1c1c1)];
    [self.bottomLine setHidden:YES];
}
- (void)clickMenuItem{
    [self setSelected:YES];
}



- (void)setTitle:(NSString * )title{
    
    CGSize fontSize = AWFontSize(title, self.titleLabel.font);
    [self.titleLabel setText:title];
    [self.titleLabel setFrame:CGRectMake(0, 0, fontSize.width, fontSize.height)];
    [self.button setFrame:CGRectMake(0, 0, fontSize.width, fontSize.height)];
    
    [self setSelected:self.selected];
}

- (void)setSelected:(BOOL) selected{
    _selected = selected;
    
    // 메뉴 아이템 상태 변화
    if(_selected){
        [self stateSelected];
        // 메뉴 델리게이트 처리
        if(_delegate){
            [_delegate selectedItem:self];
        }
    }else{
        [self stateUnSelected];
    }
}

@end
