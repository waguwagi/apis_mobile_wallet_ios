//
//  AWMenuItem.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 07/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol AWMenuItemDelegate;

@interface AWMenuItem : UIView
@property (assign, nonatomic) id<AWMenuItemDelegate> delegate;
@property (assign, nonatomic) BOOL selected;
- (void)setTitle:(NSString * )title;
- (void)setSelected:(BOOL) selected;
@end

@protocol AWMenuItemDelegate<NSObject>
- (void)selectedItem:(AWMenuItem * )item;
@end
NS_ASSUME_NONNULL_END
