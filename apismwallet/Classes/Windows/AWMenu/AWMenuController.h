//
//  AWMenuController.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 08/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AWMenuItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface AWMenuController : NSObject

@property (strong, nonatomic) NSMutableArray * menuItemList;
- (void)addMenuItem:(AWMenuItem * )item;
- (void)selectItem:(AWMenuItem * )item;
@end

NS_ASSUME_NONNULL_END
