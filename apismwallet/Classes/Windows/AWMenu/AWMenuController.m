//
//  AWMenuController.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 08/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWMenuController.h"

@interface AWMenuController() <AWMenuItemDelegate>
@end
@implementation AWMenuController

- (id)init{
    self = [super init];
    if(self){
        
        self.menuItemList = [[NSMutableArray alloc]init];
        
    }
    return self;
}
- (void)addMenuItem:(AWMenuItem * _Nonnull)item{
    [item setDelegate:self];
    [_menuItemList addObject:item];
}

- (void)selectItem:(AWMenuItem * )item{
    NSUInteger listCount = [_menuItemList count];
    for(int i=0; i<listCount; i++){
        if(_menuItemList[i] == item){
            [_menuItemList[i] setSelected:YES];
            break;
        }
    }
}

#pragma mark - AWMenuItemDelegate
- (void)selectedItem:(AWMenuItem * )item{
    NSUInteger listCount = [_menuItemList count];
    for(int i=0; i<listCount; i++){
        if(_menuItemList[i] == item){
        }else{
            [_menuItemList[i] setSelected:NO];
        }
    }
}

@end
