//
//  AWSecretButton.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 08/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWSecretButton.h"

@interface AWSecretButton()
@property BOOL active;

@end
@implementation AWSecretButton

-(void)initData{
    self.onImage = [UIImage imageNamed:@"btn_on"];
    self.offImage = [UIImage imageNamed:@"btn_off"];
    
    self.active = YES;
}

-(void)initLayout
{
    // ON/OFF 이미지 초기화
    if(self.active){
        [self showValue];
    }else{
        [self hiddenValue];
    }
}

-(void)toggleShowHiddenValue{
    if(self.active){
        [self hiddenValue];
    }else{
        [self showValue];
    }
}
-(void)showValue
{
    self.active = YES;
    [self.imageView setImage:[UIImage imageNamed:@"btn_on"]];
}
-(void)hiddenValue
{
    self.active = NO;
    [self.imageView setImage:[UIImage imageNamed:@"btn_off"]];
}
-(BOOL)isShowValue
{
    return self.active;
}

@end
