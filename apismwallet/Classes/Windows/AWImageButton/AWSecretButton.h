//
//  AWSecretButton.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 08/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface AWSecretButton : UIButton

@property UIImage * onImage;
@property UIImage * offImage;

-(void)toggleShowHiddenValue;
-(void)showValue;
-(void)hiddenValue;
-(BOOL)isShowValue;

@end

NS_ASSUME_NONNULL_END
