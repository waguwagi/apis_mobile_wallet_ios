//
//  AWStepView.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWStepView.h"

@implementation AWStepView

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initData];
        [self initLayout];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
        [self initLayout];
    }
    return self;
}

- (void)initData{
    [self initLabel];
}
- (void)initLayout{
    self.backgroundColor = UIColor.clearColor;
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = AWWidth(8);
    
}

- (void)initLabel{
    self.label = [[AWLabelNaviNum alloc] init];
    [self addSubview: self.label];
    
    [self.label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
}

- (void)drawRect:(CGRect)rect{
    
    
    [self.label setText:self.title];
    //self.label.font = [UIFont systemFontOfSize:AWWidth(10)];
    
    if(self.active){
        [self.label setTextColor:AWTextColorWhite()];
        self.layer.backgroundColor = AWBGColorRed().CGColor;
    }else{
        [self.label setTextColor:AWTextColorBlack()];
        self.layer.backgroundColor = AWBGColorWhite().CGColor;
    }
}

- (void)setActive:(BOOL)active{
    _active = active;
    if(_active){
        [self.label setTextColor:AWTextColorWhite()];
        self.layer.backgroundColor = AWBGColorRed().CGColor;
    }else{
        [self.label setTextColor:AWTextColorBlack()];
        self.layer.backgroundColor = AWBGColorWhite().CGColor;
    }
}

@end
