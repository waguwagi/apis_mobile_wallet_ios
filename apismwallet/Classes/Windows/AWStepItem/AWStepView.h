//
//  AWStepView.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AWStepView : UIView

@property AWLabelNaviNum * label;
@property IBInspectable NSString * title;
@property IBInspectable (assign, nonatomic) BOOL active;

@end

NS_ASSUME_NONNULL_END
