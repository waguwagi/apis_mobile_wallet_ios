//
//  AWPrivateKeyBackup.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWPrivateKeyBackup.h"

@interface AWPrivateKeyBackup()
{
    BOOL secret;
    NSString * strPrivateKey;
}

@property (weak, nonatomic) IBOutlet UIImageView *secretIcon;

@end
@implementation AWPrivateKeyBackup

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.containerCopyView.layer.borderColor = AWLineColorGray().CGColor;
    self.containerCopyView.layer.borderWidth = 1.0f;
    
    self.privateKey.textContainer.lineFragmentPadding = 0;
    self.privateKey.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.privateKey.font = [UIFont systemFontOfSize:AWWidth(12) weight:UIFontWeightRegular];
    [self.privateKey setScrollEnabled:NO];
    
    [self setSecret:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)copy:(id)sender {
    [self.textCopyButton setCopyString:strPrivateKey];
    if(self.delegate){
        [self.delegate copy:self text:strPrivateKey];
    }
}

- (IBAction)showQRCode:(id)sender {
    if(self.delegate){
        [self.delegate showQRCode:self privateKey:strPrivateKey];
    }
}

- (void)setSecret:(BOOL)isSecret{
    secret = isSecret;
    if(secret){
        _secretIcon.image = [UIImage imageNamed:@"btnOpenEye"];
        _privateKey.text = strPrivateKey;
    }else{
        _secretIcon.image = [UIImage imageNamed:@"btnCloseEye"];
        strPrivateKey = _privateKey.text;
        NSString * temp = [StringManager getPassword:_privateKey.text mark:@"-"] ;
        _privateKey.text = temp;
        
    }
}

- (IBAction)toggleSecret:(id)sender {
    [self setSecret:!secret];
}



+(double)estimatedRowHeight{
    return AWWidth(187);
}

@end
