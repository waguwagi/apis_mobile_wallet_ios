//
//  AWPrivateKeyBackup.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AWPrivateKeyBackupDelegate;

@interface AWPrivateKeyBackup : BaseTableViewItem

@property (assign, nonatomic) id<AWPrivateKeyBackupDelegate> delegate;
@property (weak, nonatomic) IBOutlet AWLabelMainTitle *titleLabel;
@property (weak, nonatomic) IBOutlet AWCopyButton *textCopyButton;
@property (weak, nonatomic) IBOutlet UITextView *privateKey;
@property (weak, nonatomic) IBOutlet UIView *containerCopyView;

@end

@protocol AWPrivateKeyBackupDelegate <NSObject>
- (void)copy:(AWPrivateKeyBackup * )view text:(NSString *)text;
- (void)showQRCode:(AWPrivateKeyBackup * )view privateKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
