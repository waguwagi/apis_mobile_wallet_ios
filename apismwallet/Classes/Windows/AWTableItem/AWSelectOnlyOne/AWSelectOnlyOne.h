//
//  AWSelectOnlyOne.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AWSelectOnlyOneDelegate;

@interface AWSelectOnlyOne : BaseTableViewItem

@property (assign, nonatomic) id<AWSelectOnlyOneDelegate> delegate;
@property (weak, nonatomic) IBOutlet AWSelectButton *keystoreFileButton;
@property (weak, nonatomic) IBOutlet AWSelectButton *privateKeyButton;

@end

@protocol AWSelectOnlyOneDelegate <NSObject>
- (void)selected:(AWSelectButton * )button index:(NSInteger)index;
@end

NS_ASSUME_NONNULL_END
