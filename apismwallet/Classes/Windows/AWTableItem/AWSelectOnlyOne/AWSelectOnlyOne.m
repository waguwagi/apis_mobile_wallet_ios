//
//  AWSelectOnlyOne.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWSelectOnlyOne.h"

@implementation AWSelectOnlyOne

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)selected:(id)sender {
    
    [self setSelectedButton:sender];
}

- (void) setSelectedButton:(AWSelectButton *)sender{
    
    [_keystoreFileButton setSelected:NO];
    [_privateKeyButton setSelected:NO];
    
    if(sender == _keystoreFileButton){
        [_keystoreFileButton setSelected:YES];
        
        if(_delegate){
            [self.delegate selected:_keystoreFileButton index:0];
        }
        
    }else if(sender == _privateKeyButton){
        [_privateKeyButton setSelected:YES];
        
        if(_delegate){
            [self.delegate selected:_privateKeyButton index:1];
        }
    }
}

+ (double)estimatedRowHeight{
    return AWWidth(138);
}

@end
