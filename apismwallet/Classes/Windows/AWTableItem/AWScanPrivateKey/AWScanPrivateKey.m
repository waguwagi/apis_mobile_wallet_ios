//
//  AWScanPrivateKey.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 21/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWScanPrivateKey.h"

@implementation AWScanPrivateKey

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPrivateKey:(NSString *)privateKey{
    self.privateKeyTextView.text = privateKey;
}

- (IBAction)openScanPopup:(id)sender {
    if(_delegate){
        [self.delegate openScanPopupAWScanPrivateKeyDelegate];
    }
}

+(double)estimatedRowHeight{
    return AWWidth(121);
}

@end
