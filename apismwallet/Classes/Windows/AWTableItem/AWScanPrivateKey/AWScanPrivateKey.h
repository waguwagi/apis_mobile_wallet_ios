//
//  AWScanPrivateKey.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 21/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AWScanPrivateKeyDelegate;

@interface AWScanPrivateKey : BaseTableViewItem

@property (assign, nonatomic) id<AWScanPrivateKeyDelegate> delegate;
@property (weak, nonatomic) IBOutlet AWTextViewPrivateKey *privateKeyTextView;

- (void)setPrivateKey:(NSString *)privateKey;

@end

@protocol AWScanPrivateKeyDelegate <NSObject>
- (void)openScanPopupAWScanPrivateKeyDelegate;
@end

NS_ASSUME_NONNULL_END
