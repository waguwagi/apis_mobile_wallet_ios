
//
//  AWInfoMessageSub3.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWInfoMessageSub3.h"

@implementation AWInfoMessageSub3

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (double)estimatedRowHeight{
    return AWWidth(52);
}

@end
