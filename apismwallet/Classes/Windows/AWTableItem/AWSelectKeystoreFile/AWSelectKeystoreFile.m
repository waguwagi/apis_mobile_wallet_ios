//
//  AWSelectKeystoreFile.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 21/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWSelectKeystoreFile.h"

@implementation AWSelectKeystoreFile

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(double)estimatedRowHeight{
    return AWWidth(95);
}
@end
