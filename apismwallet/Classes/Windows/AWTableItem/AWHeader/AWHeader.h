//
//  AWHeader.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseView.h"
#import "AWStepView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AWHeaderDelegate;

@interface AWHeader : BaseView
@property (assign, nonatomic) id<AWHeaderDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
@property (weak, nonatomic) IBOutlet AWStepView *nav1;
@property (weak, nonatomic) IBOutlet AWStepView *nav2;
@property (weak, nonatomic) IBOutlet AWStepView *nav3;

@property (assign, nonatomic) BOOL naviUesd;

- (void) setStep:(NSInteger) step subTitle:(NSString *)subTitle;
@end

@protocol AWHeaderDelegate<NSObject>
- (void)backAWHeaderDelegate;
@end

NS_ASSUME_NONNULL_END
