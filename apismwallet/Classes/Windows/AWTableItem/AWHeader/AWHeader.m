//
//  AWHeader.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWHeader.h"

@implementation AWHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.nav1 setHidden:YES];
    [self.nav2 setHidden:YES];
    [self.nav3 setHidden:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setNaviUesd:(BOOL)naviUesd{
    _naviUesd = naviUesd;
    
    if(_naviUesd){
        [self.nav1 setHidden:NO];
        [self.nav2 setHidden:NO];
        [self.nav3 setHidden:NO];
    }
}

- (void) setStep:(NSInteger) step subTitle:(NSString *)subTitle{
    if(self.naviUesd){
        [self.nav1 setActive:NO];
        [self.nav2 setActive:NO];
        [self.nav3 setActive:NO];
        
        if(step == 0){
            [UIView transitionWithView: self.nav1 duration: 0.2f options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void) {
                                [self.nav1 setActive:YES];
                            } completion: nil];
        }else if(step == 1){
            [UIView transitionWithView: self.nav2 duration: 0.2f options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void) {
                                [self.nav2 setActive:YES];
                            } completion: nil];
        }else if(step == 2){
            [UIView transitionWithView: self.nav3 duration: 0.2f options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void) {
                                [self.nav3 setActive:YES];
                            } completion: nil];
        }
    }
    
    [UIView transitionWithView: self.subTitle duration: 0.2f options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void) {
                        [self.subTitle setText:subTitle];
                    } completion: nil];
}

- (IBAction)back:(id)sender {
    if(self.delegate){
        [self.delegate backAWHeaderDelegate];
    }
}
+ (double)estimatedRowHeight{
    return AWWidth(102);
}

@end
