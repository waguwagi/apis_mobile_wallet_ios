//
//  AWDownloadKeystoreFile.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWDownloadKeystoreFile.h"

@implementation AWDownloadKeystoreFile

- (void)awakeFromNib {
    [super awakeFromNib];

    [self.titleLabel setText:AWString(@"Download Keystore file")];
    [self.downloadButton.titleLabel setFont:[UIFont systemFontOfSize:AWWidth(14)]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (IBAction)download:(id)sender {
    if(self.delegate){
        [self.delegate awDownloadKeystoreFileDelegateDownloadKeyStoreFile:self];
    }
}

+ (double)estimatedRowHeight{
    return AWWidth(95);
}

@end
