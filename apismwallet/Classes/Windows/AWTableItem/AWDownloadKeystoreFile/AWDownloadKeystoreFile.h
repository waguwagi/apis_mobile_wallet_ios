//
//  AWDownloadKeystoreFile.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AWDownloadKeystoreFileDelegate;

@interface AWDownloadKeystoreFile : BaseTableViewItem
@property (assign, nonatomic) id<AWDownloadKeystoreFileDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet AWRedRectButton *downloadButton;

@end

@protocol AWDownloadKeystoreFileDelegate<NSObject>
- (void)awDownloadKeystoreFileDelegateDownloadKeyStoreFile:(AWDownloadKeystoreFile * )cell;
@end

NS_ASSUME_NONNULL_END
