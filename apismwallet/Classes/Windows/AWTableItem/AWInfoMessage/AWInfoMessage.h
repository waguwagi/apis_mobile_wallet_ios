//
//  AWInfoMessage.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 16/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AWInfoMessage : BaseTableViewItem
@property (weak, nonatomic) IBOutlet UILabel *message;
- (void)setText:(NSString * _Nullable)text;
@end

NS_ASSUME_NONNULL_END
