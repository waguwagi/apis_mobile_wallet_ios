//
//  AWInfoMessage.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 16/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWInfoMessage.h"

@implementation AWInfoMessage

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.message setFont:[UIFont systemFontOfSize:AWWidth(12)]];
    
}
- (void)setText:(NSString *)text{
    self.message.text = text;
}

+ (double)estimatedRowHeight{
    return AWWidth(51);
}
@end
