//
//  AWWalletConfirmPasswordField.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AWWalletConfirmPasswordField : BaseTableViewItem
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet BaseTextField *password;
@property (weak, nonatomic) IBOutlet BaseTextField *repassword;

@end

NS_ASSUME_NONNULL_END
