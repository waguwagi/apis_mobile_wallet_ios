//
//  AWWalletConfirmPasswordField.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWWalletConfirmPasswordField.h"

@implementation AWWalletConfirmPasswordField

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)secretPassword:(id)sender {
    _password.secureTextEntry = !_password.secureTextEntry;
}
+ (double)estimatedRowHeight{
    return AWWidth(155);
}

@end
