//
//  AWWalletNameField.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AWWalletNameField : BaseTableViewItem

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet BaseTextField *textField;

@end

NS_ASSUME_NONNULL_END
