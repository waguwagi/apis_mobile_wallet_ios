//
//  AWFooter.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWFooter.h"

@implementation AWFooter

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.button.titleLabel setFont:[UIFont systemFontOfSize:AWWidth(14)]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)next:(id)sender {
    if(self.delegate){
        [self.delegate nextAWFooterDelegate];
    }
}
+ (double)estimatedRowHeight{
    return AWWidth(56);
}

@end
