//
//  AWFooter.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AWFooterDelegate;

@interface AWFooter : BaseView
@property (assign, nonatomic) id<AWFooterDelegate> delegate;
@property (weak, nonatomic) IBOutlet AWRedRectButton *button;

@end

@protocol AWFooterDelegate<NSObject>
- (void)nextAWFooterDelegate;
@end

NS_ASSUME_NONNULL_END
