//
//  AWTextViewPrivateKey.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 21/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWTextViewPrivateKey.h"

#define MAX_LENGTH 64

@interface AWTextViewPrivateKey() <UITextViewDelegate>

@end
@implementation AWTextViewPrivateKey

- (void)initData{
    [super initData];
    
    UIEdgeInsets bounds = UIEdgeInsetsMake(AWWidth(20), AWWidth(20), AWWidth(20), AWWidth(49));
    if(SCREEN_WIDTH < 375){
        bounds = UIEdgeInsetsMake(AWWidth(20), AWWidth(20), AWWidth(20), AWWidth(44));
    }
    
    self.textContainerInset = bounds;
    self.textContainer.lineFragmentPadding = 0;
    self.textContainer.maximumNumberOfLines = 2;
    self.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
    self.delegate = self;
}

#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    if(newLength > MAX_LENGTH){
        NSUInteger emptySpace = MAX_LENGTH - (textView.text.length - range.length);
        textView.text = [[[textView.text substringToIndex:range.location]
                          stringByAppendingString:[text substringToIndex:emptySpace]]
                         stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        return NO;
    }
    
    return YES;
}
@end
