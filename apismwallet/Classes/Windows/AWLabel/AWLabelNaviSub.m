//
//  AWLabelNaviSub.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWLabelNaviSub.h"

@implementation AWLabelNaviSub

- (void)initData{
    self.titleFont = [UIFont systemFontOfSize:AWWidth(12) weight:UIFontWeightSemibold];
    self.textColor = AWTextColorBlack();
}

@end
