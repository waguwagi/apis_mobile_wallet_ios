//
//  AWLabelNavi.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWLabelNavi.h"

@implementation AWLabelNavi

- (void)initData{
    self.titleFont = [UIFont systemFontOfSize:AWWidth(18) weight:UIFontWeightSemibold];
    self.textColor = AWTextColorBlack();
}

@end
