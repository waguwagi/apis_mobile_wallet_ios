//
//  AWLabelPopupTitle.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWLabelPopupTitle.h"

@implementation AWLabelPopupTitle

- (void)initData{
    self.titleFont = [UIFont systemFontOfSize:AWWidth(16) weight:UIFontWeightSemibold];
    self.textColor = AWTextColorBlack();
}

@end
