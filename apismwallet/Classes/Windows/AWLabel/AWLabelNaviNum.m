//
//  AWLabelNaviNum.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWLabelNaviNum.h"

@implementation AWLabelNaviNum

- (void)initData{
    self.titleFont = [UIFont systemFontOfSize:AWWidth(10) weight:UIFontWeightBold];
    self.textColor = AWTextColorBlack();
}

@end
