
//
//  AWLabelMainTitle.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWLabelMainTitle.h"

@implementation AWLabelMainTitle

- (void)initData{
    self.titleFont = [UIFont systemFontOfSize:AWWidth(14) weight:UIFontWeightMedium];
    self.textColor = AWTextColorBlack();
}

@end
