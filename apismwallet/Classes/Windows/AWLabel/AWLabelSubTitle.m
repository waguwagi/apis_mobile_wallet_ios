//
//  AWLabelSubTitle.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWLabelSubTitle.h"

@implementation AWLabelSubTitle

- (void)initData{
    self.titleFont = [UIFont systemFontOfSize:AWWidth(12) weight:UIFontWeightRegular];
    self.textColor = AWTextColorGray();
}

@end
