//
//  AWCopyButton.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 03/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWLineRectButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface AWCopyButton : AWLineRectButton

- (void)setCopyString:(NSString *)copy;

@end

NS_ASSUME_NONNULL_END
