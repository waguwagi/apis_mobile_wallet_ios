//
//  AWSelectButton.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 21/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWSelectButton.h"

@implementation AWSelectButton

- (void)initData{
    [super initData];
    
    super.cornerRadius = 0.0f;
    [self.titleLabel setFont:[UIFont systemFontOfSize:AWWidth(14) weight:UIFontWeightRegular]];
    
    // 아이콘 및 텍스트 위치
    [self setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    // 아이콘 이미지
    [self setImage:[UIImage imageNamed:@"icNonecheckWbtn"] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:@"icCheckRedbtn"] forState:UIControlStateSelected];
    [self setImage:[UIImage imageNamed:@"icCheckRedbtn"] forState:UIControlStateHighlighted];
    [self setImage:[UIImage imageNamed:@"icNonecheckWbtn"] forState:UIControlStateSelected&UIControlStateHighlighted];
    
    
    
}

- (void)initLayout{
    [super initLayout];
    
    // 텍스트 색상
    [self setTitleColor:AWTextColorGray() forState:UIControlStateNormal];
    [self setTitleColor:AWTextColorWhite() forState:UIControlStateHighlighted];
    [self setTitleColor:AWTextColorWhite() forState:UIControlStateSelected];
    [self setTitleColor:AWTextColorGray() forState:UIControlStateSelected&UIControlStateHighlighted];
    
    [self setHighlighted:self.isHighlighted];
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    
    self.layer.borderWidth = 0.0f;
    if(selected){
        [self setBackgroundColor:AWBGColorRed()];
    }else{
        [self setBackgroundColor:AWBGColorWhite()];
        
        self.layer.borderWidth = 1.0f;
        self.layer.borderColor = AWLineColorGray().CGColor;
    }
}

- (void)setHighlighted:(BOOL)highlighted{
    [super setHighlighted:highlighted];
    
    self.layer.borderWidth = 0.0f;
    if(highlighted){
        
        if(self.isSelected){
            [self setBackgroundColor:AWBGColorDarkRed()];
            self.layer.borderColor = AWLineColorDarkRed().CGColor;
        }else{
            [self setBackgroundColor:AWBGColorGray()];
            self.layer.borderColor = AWLineColorGray().CGColor;
        }
    }else{
        
        if(self.isSelected){
            [self setBackgroundColor:AWBGColorRed()];
            self.layer.borderColor = AWLineColorRed().CGColor;
        }else{
            self.layer.borderWidth = 1.0f;
            [self setBackgroundColor:AWBGColorWhite()];
            self.layer.borderColor = AWLineColorGray().CGColor;
        }
    }
    
    
}

@end
