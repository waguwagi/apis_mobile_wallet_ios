//
//  AWCopyButton.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 03/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWCopyButton.h"

@implementation AWCopyButton

- (void)initData{
    [super initData];
    
    CGRect frame = self.frame;
    frame.size.width = 48;
    frame.size.height = 24;
    self.frame = frame;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.titleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    [self setTitle:AWString(@"Copy") forState:UIControlStateNormal];
    [self setTitleColor:AWTextColorBlack() forState:UIControlStateNormal];
}
		
- (void)setCopyString:(NSString *)copy{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = copy;
}
@end
