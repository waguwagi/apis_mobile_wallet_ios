//
//  AWLineRectButton.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 03/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWLineRectButton.h"

@implementation AWLineRectButton

- (void)initData{
    [super initData];
    
    super.cornerRadius = 4.0f;
    super.borderWidth = 1.0f;
    super.titleNormalColor = AWColorFromHexRGB(0x2b2b2b);
    super.titleHighlightedColor = AWColorFromHexRGB(0xffffff);
    super.backgroundNormalColor = AWColorFromHexRGB(0xffffff);
    super.backgroundHighlightedColor = AWColorFromHexRGB(0xd24037);
    super.borderNormalColor = AWColorFromHexRGB(0xe2e2e2);
    super.borderHighlightedColor = AWColorFromHexRGB(0xd24037);
    
    super.contentEdgeInsets = UIEdgeInsetsMake(AWWidth(5), AWWidth(10), AWWidth(5), AWWidth(10));
    super.titleLabel.numberOfLines = 1;
    super.titleLabel.adjustsFontSizeToFitWidth = NO;
}

@end
