//
//  AWGrayRectButton.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 03/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWGrayRectButton.h"

@implementation AWGrayRectButton

- (void)initData{
    [super initData];
    
    super.cornerRadius = 0.0f;
    super.titleNormalColor = AWColorFromHexRGB(0x999999);
    super.titleHighlightedColor = AWColorFromHexRGB(0x999999);
    super.backgroundNormalColor = AWColorFromHexRGB(0xf7f8fc);
    super.backgroundHighlightedColor = AWColorFromHexRGB(0xd8d8d8);
    [self.titleLabel setFont:[UIFont systemFontOfSize:AWWidth(14)]];
    
}

@end
