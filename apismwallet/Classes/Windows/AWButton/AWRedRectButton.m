//
//  AWRedRectButton.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 03/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "AWRedRectButton.h"


@implementation AWRedRectButton

- (void)initData{
    [super initData];
    
    super.cornerRadius = 0.0f;
    super.titleNormalColor = AWColorFromHexRGB(0xffffff);
    super.titleHighlightedColor = AWColorFromHexRGB(0xffffff);
    super.backgroundNormalColor = AWColorFromHexRGB(0xd24037);
    super.backgroundHighlightedColor = AWColorFromHexRGB(0x910000);
    [self.titleLabel setFont:[UIFont systemFontOfSize:AWWidth(14) weight:UIFontWeightRegular]];
}


@end
