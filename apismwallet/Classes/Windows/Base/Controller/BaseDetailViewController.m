//
//  BaseDetailViewController.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 11/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseDetailViewController.h"

@interface BaseDetailViewController ()<UITextFieldDelegate>

@property (assign, nonatomic) float keyboardOffsetY;

@end

@implementation BaseDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self enableTouchDismisKeyboard:YES];
    
    self.tableView.backgroundColor = AWBGColorWhite();
    self.safeTop.backgroundColor = AWBGColorWhite();
    
    // Safe View 초기화
    [self initSafeHeaderLayer];
    [self initSafeFooterLayer];
    
    // Header 초기화
    [self initHeader];
    
    // Footer 초기화
    [self initFooter];
}

- (void)initHeader{
    self.header = [[[NSBundle mainBundle] loadNibNamed:@"AWHeader" owner:self options:nil] firstObject];
    self.header.backgroundColor = AWBGColorWhite();
}


- (void)initFooter{
    self.footer = [[[NSBundle mainBundle] loadNibNamed:@"AWFooter" owner:self options:nil] firstObject];
    self.footer.backgroundColor = AWBGColorWhite();
}


-(void) initSafeHeaderLayer {
    self.safeTop = [[UIView alloc] init];
    self.safeTop.backgroundColor = UIColor.clearColor;
    
    [self.view addSubview:self.safeTop];
    [self addConstraintsForSafeTop];
    
}
-(void) initSafeFooterLayer {
    self.safeBottom = [[UIView alloc] init];
    self.safeBottom.backgroundColor = UIColor.clearColor;
    
    [self.view addSubview:self.safeBottom];
    [self addConstraintsForSafeBottom];
}


-(void) addConstraintsForSafeTop {
    if (@available(iOS 11.0, *)) {
        [self.safeTop setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.safeTop attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.safeTop attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.safeTop attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.safeTop attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
    } else {
        CGRect frame = self.view.frame;
        frame.size.height = 20;
        self.safeTop.frame = frame;
    }
}

-(void) addConstraintsForSafeBottom {
    if (@available(iOS 11.0, *)) {
        [self.safeBottom setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.safeBottom attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.safeBottom attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.safeBottom attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.safeBottom attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
    } else {
        CGRect frame = self.view.frame;
        frame.size.height = 0;
        self.safeBottom.frame = frame;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self tableViewCellNibNames].count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == self.tableViewCellNibNames.count - 1){
        CGFloat height = self.tableView.frame.size.height;
        // header height
        if(self.header){
            height -= [AWHeader estimatedRowHeight];
        }
        
        // footer height
        if(self.footer){
            height -= [AWFooter estimatedRowHeight];
        }
        
        // contents height
        for(int i=0 ;i<self.tableViewCellNibNames.count-1; i++){
            height -= [NSClassFromString(self.tableViewCellNibNames[i]) estimatedRowHeight];
            if(height < 0){
                break;
            }
        }
        if (@available(iOS 11.0, *)) {
        }else{
            height -= 20; //status height
        }
        
        if(height < AWWidth(20)){
            height = AWWidth(20);
        }
        
        return height;
    }else{
        return [NSClassFromString(self.tableViewCellNibNames[indexPath.row]) estimatedRowHeight];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = nil;
    switch (indexPath.row) {
        default:{
            AWBlankCell * tempCell = [tableView dequeueReusableCellWithIdentifier:@"AWBlankCell" forIndexPath:indexPath];
            //tempCell.backgroundColor = AWColorFromHexRGB(0x990000);
            cell = (UITableViewCell * )tempCell;
        }
    }
    
    cell.selectionStyle = UITableViewCellAccessoryNone;
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.header;
}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(self.header){
        return [AWHeader estimatedRowHeight];
    }else{
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return self.footer;
}
-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(self.footer){
        return [AWFooter estimatedRowHeight];
    }else{
        return 0;
    }
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    // Cell 이동
    UITableViewCell *editCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:textField.tag inSection:0]];
    _keyboardOffsetY =  self.tableView.contentOffset.y - editCell.frame.origin.y;
    for (UITableViewCell *cell in [self.tableView visibleCells]) {
        
        [UIView animateWithDuration:0.3 animations:^{
            cell.frame = CGRectOffset(cell.frame, 0, (self.header.frame.size.height + self->_keyboardOffsetY));
        }];
    }
    
    // 편집 중에 scroll 금지
    self.tableView.scrollEnabled = NO;
}

- (void)textFieldDidEndEditing:(UITextField*)textField{
    
    for (UITableViewCell *cell in [self.tableView visibleCells]) {
        [UIView animateWithDuration:0.3 animations:^{
            
            cell.frame = CGRectOffset(cell.frame, 0, - (self.header.frame.size.height + self->_keyboardOffsetY));
        }];
    }
    
    // 편집 후 scroll 가능
    self.tableView.scrollEnabled = YES;
}

@end
