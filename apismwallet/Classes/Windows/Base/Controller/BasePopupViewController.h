//
//  BasePopupViewController.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 03/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BasePopupViewController : BaseViewController

@property (nonatomic) BasePopupModel * model;
@property (assign, nonatomic) CGFloat backgroundDelay;

@end

NS_ASSUME_NONNULL_END
