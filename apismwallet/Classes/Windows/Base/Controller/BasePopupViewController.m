//
//  BasePopupViewController.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 03/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BasePopupViewController.h"

@interface BasePopupViewController () {
    UIView *dimBackgroundUIView;
}

@end

@implementation BasePopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    // 딤 초기화
    [self initDimLayer];
}


-(void) initDimLayer {
    dimBackgroundUIView = [[UIView alloc] init];
    
    [self.view addSubview:dimBackgroundUIView];
    [self addConstraintsForDimView];
    self->dimBackgroundUIView.alpha = 0.0;
    self->dimBackgroundUIView.backgroundColor = [UIColor blackColor];
    
    // 딤 zindex 맨 밑으로 이동
    [self.view sendSubviewToBack:dimBackgroundUIView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [UIView animateWithDuration:0.3 delay:_backgroundDelay options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self->dimBackgroundUIView.alpha = 0.5;
                     } completion:nil];
    
}


-(void) addConstraintsForDimView {
    
    [dimBackgroundUIView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dimBackgroundUIView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dimBackgroundUIView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dimBackgroundUIView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dimBackgroundUIView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
}


- (IBAction)closePopup:(id)sender {
    [UIView animateWithDuration:0.1 animations:^{
        self->dimBackgroundUIView.alpha = 0;
    }];
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
