//
//  BaseDetailViewController.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 11/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseDetailViewController : BaseViewController

@property UIView *safeTop, *safeBottom;

/**
 @details BaseViewController를 상속받는 하위 ViewController에서 사용할 UITableView의 Header
 */
@property (strong, nonatomic, nullable) AWHeader *header;

/**
 @details BaseViewController를 상속받는 하위 ViewController에서 사용할 UITableView의 Footer
 */
@property (strong, nonatomic, nullable) AWFooter *footer;

@end

NS_ASSUME_NONNULL_END
