//
//  BaseViewController.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 04/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE @interface BaseViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

/**
 @details BaseViewController를 상속받는 하위 ViewController에서 사용할 UITableView
 */
@property (strong, nonatomic) IBOutlet UITableView *tableView;

/**
 @details 테이블뷰에 등록할 Xib 이름을 가져와 위 tableView에 등록
 */
@property (copy, nonatomic, readonly) NSArray<NSString *> *tableViewCellNibNamesAll;
@property (copy, nonatomic, readonly) NSArray<NSString *> *tableViewCellClassNamesAll;
@property (copy, nonatomic, readonly) NSArray<NSString *> *tableViewCellNibNames;
@property (copy, nonatomic, readonly) NSArray<NSString *> *tableViewCellClassNames;
@property (assign, nonatomic, readonly) BOOL addKeyboardHidingTapGestureRecogniger;

- (void) enableTouchDismisKeyboard:(BOOL)value;
- (void) initTableView;
- (void) showRightMenu;
- (void) hideRightMenu;
- (void) rightMenuSwipeGestureEnabled:(BOOL)enabled;
- (void) back;

- (void) toCreateWallet;
- (void) toLoadWallet;

- (void) showPopup:(BaseViewController *)popupViewController completion:(void (^ __nullable)(void))completion;
- (void) showPopupAnimation:(BaseViewController *)popupViewController completion:(void (^ __nullable)(void))completion;

@end

NS_ASSUME_NONNULL_END
