//
//  BaseViewController.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 04/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseViewController.h"
#import "LGSideMenuController.h"
#import "CreateWalletViewController.h"
#import "loadWalletViewController.h"


@interface BaseViewController ()<UIGestureRecognizerDelegate>

@property (assign, nonatomic) double lastUpdateTime;
@property (assign, nonatomic) bool isShowKeyboard;
@property (assign, nonatomic) float keyboardHeight;
@property (assign, nonatomic) CGRect visibleRect;
@property (assign, nonatomic) UIEdgeInsets oldTableViewInset;

@property (strong, nonatomic) UITapGestureRecognizer *keyboardHidingTapGesture;

@end

@implementation BaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self addKeyboardHidingTapGestureRecogniger]) {
        
        self.keyboardHidingTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:NSSelectorFromString(@"dismissKeyboard")];
        [self.view addGestureRecognizer:self.keyboardHidingTapGesture];
    }
    
    [self initTableView];
    [self initChilds];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void) initTableView{
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.bounces = NO;
}

- (void) initChilds
{
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self registerTableViewCellNibNames];
    [self registerTableViewCellClassNames];
}



#pragma mark - publics
- (void) back{
    if ([self.navigationController.viewControllers count] > 1 && self.navigationController.viewControllers.firstObject != self) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)rightMenuSwipeGestureEnabled:(BOOL)enabled{
    LGSideMenuController * sideMenuController = (LGSideMenuController *)[self parentViewController];
    sideMenuController.rightViewSwipeGestureEnabled = enabled;
}

- (void)showRightMenu{
    LGSideMenuController * menuController = (LGSideMenuController *)[self parentViewController];
    [menuController showRightViewAnimated];
}

- (void)hideRightMenu{
    LGSideMenuController * menuController = (LGSideMenuController *)[self parentViewController];
    [menuController hideRightViewAnimated];
}

#pragma mark - Move page

- (void)toCreateWallet{
    [self hideRightMenu];
    
    CreateWalletViewController *createWalletViewController = [[CreateWalletViewController alloc] initWithNibName:@"CreateWalletViewController" bundle:nil];
    [self.navigationController pushViewController:createWalletViewController animated:YES];
}

- (void)toLoadWallet{
    [self hideRightMenu];
    
    LoadWalletViewController *loadWalletViewController = [[LoadWalletViewController alloc] initWithNibName:@"LoadWalletViewController" bundle:nil];
    [self.navigationController pushViewController:loadWalletViewController animated:YES];
}

#pragma mark - Popup
- (void)showPopup:(BaseViewController *)popupViewController completion:(void (^ __nullable)(void))completion{
    [popupViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [popupViewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    [topController presentViewController:popupViewController animated:YES completion:completion];
}
- (void)showPopupAnimation:(BaseViewController *)popupViewController completion:(void (^ __nullable)(void))completion{
    [popupViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [popupViewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    [topController presentViewController:popupViewController animated:YES completion:completion];
}


#pragma mark - Keyboard


- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}

#pragma mark - TapGesture

- (void) enableTouchDismisKeyboard:(BOOL)value
{
    if (value)
    {
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTabGestureHandler:)];
        [gestureRecognizer setCancelsTouchesInView:NO];
        [self.view addGestureRecognizer:gestureRecognizer];
    }
    else
    {
        for (id gesture in [self.view gestureRecognizers])
        {
            if ([gesture class] == [UITapGestureRecognizer class])
            {
                [self.view removeGestureRecognizer:gesture];
                break;
            }
        }
    }
}

- (void) onTabGestureHandler:(UIGestureRecognizer *)gesture
{
    [self dismissKeyboard];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 @brief 테이블 뷰의 마지막 부분에 도달하면 다음데이터 요청하는 함수.
 @param tableView 테이블 뷰 객체
 @discussion 해당 함수를 구현하고 있는 뷰 컨트롤러 클래스에서 데이터 요청 처리 필요.
 */
- (void) ifNeedRequestNextTableViewData:(UITableView *)tableView
{
    NSLog(@"ifNeedRequestNextTableViewData call");
}

#pragma mark - Setting up

- (void)registerTableViewCellNibNames {
    
    for (NSString *nibName in self.tableViewCellNibNamesAll) {
        
        [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    }
}

- (void)registerTableViewCellClassNames {
    
    for (NSString *className in self.tableViewCellClassNamesAll) {
        
        [self.tableView registerClass:NSClassFromString(className) forCellReuseIdentifier:className];
    }
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSArray<NSString *> *)tableViewCellNibNames {
    return @[];
}

- (NSArray<NSString *> *)tableViewCellNibNamesAll {
    return [self tableViewCellNibNames];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
   
}

@end
