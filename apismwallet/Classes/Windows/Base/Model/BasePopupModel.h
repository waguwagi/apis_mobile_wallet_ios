//
//  BasePopupModel.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 18/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BasePopupModel : NSObject

@property (copy, nonatomic) NSString * privateKey;
@property (copy, nonatomic) NSString * walletName;

@end

NS_ASSUME_NONNULL_END
