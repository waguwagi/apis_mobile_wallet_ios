//
//  BaseLabel.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseLabel.h"

@implementation BaseLabel

- (void)initData{
    
    self.titleFont = [UIFont systemFontOfSize:AWWidth(14)];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initData];
        [self initLayout];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
        [self initLayout];
    }
    return self;
}

- (void)initLayout{
    [[self layer] setMasksToBounds:YES];
    
    [self setFont:self.titleFont];
}

@end
