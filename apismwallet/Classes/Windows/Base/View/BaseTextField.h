//
//  BaseTextField.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseTextField : UITextField

@property IBInspectable BOOL only;
@property IBInspectable BOOL top;
@property IBInspectable BOOL middle;
@property IBInspectable BOOL bottom;

@property CGFloat padding;
@property IBInspectable NSInteger rightPadding;


@end

NS_ASSUME_NONNULL_END
