//
//  BaseView.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 07/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initData];
        [self initLayout];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
        [self initLayout];
    }
    return self;
}

- (void)initData{
}

- (void)initLayout{
    self.backgroundColor = UIColor.clearColor;
}


- (void)dismissKeyboard
{
    [self endEditing:YES];
}

+ (double)estimatedRowHeight{
    return 0;
}

@end
