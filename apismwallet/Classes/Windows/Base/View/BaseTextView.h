//
//  AWTextView.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 03/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseTextView : UITextView

@property float borderWidth;
@property UIColor * borderColor;

@property CGFloat padding;
@property IBInspectable NSInteger rightPadding;

- (void)initData;
@end

NS_ASSUME_NONNULL_END
