//
//  BaseTextField.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 15/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseTextField.h"

@implementation BaseTextField



- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    
    _padding = AWWidth(18);
    CGFloat borderWidth = 1;
    self.font = [UIFont systemFontOfSize:12];
    
    CALayer * topLayer = [[CALayer alloc]init];
    CALayer * bottomLayer = [[CALayer alloc]init];
    CALayer * leftLayer = [[CALayer alloc]init];
    CALayer * rightLayer = [[CALayer alloc]init];
    
    [self.layer addSublayer:topLayer];
    [self.layer addSublayer:bottomLayer];
    [self.layer addSublayer:leftLayer];
    [self.layer addSublayer:rightLayer];
    
    topLayer.backgroundColor = AWColorFromRGB(226,226,226).CGColor;
    bottomLayer.backgroundColor = AWColorFromRGB(226,226,226).CGColor;
    leftLayer.backgroundColor = AWColorFromRGB(226,226,226).CGColor;
    rightLayer.backgroundColor = AWColorFromRGB(226,226,226).CGColor;
    
    if(_only){
        topLayer.frame = CGRectMake(0, 0, self.frame.size.width, borderWidth);
        bottomLayer.frame = CGRectMake(0, self.frame.size.height - borderWidth, self.frame.size.width, borderWidth);
    }else{
        if(_top){
            topLayer.frame = CGRectMake(0, 0, self.frame.size.width, borderWidth);
            bottomLayer.frame = CGRectMake(0, 0, 0, 0);
            
            bottomLayer.backgroundColor = AWColorFromRGB(244,244,244).CGColor;
        }
        
        if(_middle){
            topLayer.frame = CGRectMake(_padding, - ( borderWidth / 2.0 ), self.frame.size.width - ( _padding * 2 ), borderWidth);
            bottomLayer.frame = CGRectMake(_padding, self.frame.size.height - ( borderWidth / 2.0 ), self.frame.size.width - _padding*2, borderWidth);
            
            topLayer.backgroundColor = AWColorFromRGB(244,244,244).CGColor;
            bottomLayer.backgroundColor = AWColorFromRGB(244,244,244).CGColor;
        }
        
        if(_bottom){
            topLayer.frame = CGRectMake(_padding, - ( borderWidth / 2.0 ), self.frame.size.width - ( _padding * 2 ), borderWidth);
            bottomLayer.frame = CGRectMake(0, self.frame.size.height - borderWidth, self.frame.size.width, borderWidth);
            
            topLayer.backgroundColor = AWColorFromRGB(244,244,244).CGColor;
        }
    }
    leftLayer.frame = CGRectMake(0, 0, borderWidth, self.frame.size.height);
    rightLayer.frame = CGRectMake(self.frame.size.width - borderWidth, 0, borderWidth, self.frame.size.height);
    
}


// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    if(_rightPadding > 0){
        bounds.size.width = bounds.size.width - AWWidth(_rightPadding);
        return CGRectInset(bounds, _padding, _padding);
    }
    return CGRectInset(bounds, _padding, _padding);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    if(_rightPadding > 0){
        bounds.size.width = bounds.size.width - AWWidth(_rightPadding);
        return CGRectInset(bounds, _padding, _padding);
    }
    return CGRectInset(bounds, _padding, _padding);
}



@end
