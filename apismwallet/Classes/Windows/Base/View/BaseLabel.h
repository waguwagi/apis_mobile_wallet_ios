//
//  BaseLabel.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseLabel : UILabel

@property UIFont * titleFont;

- (void)initData;

@end

NS_ASSUME_NONNULL_END
