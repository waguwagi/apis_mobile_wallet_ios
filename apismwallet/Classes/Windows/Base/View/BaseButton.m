//
//  AWButton.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 03/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseButton.h"


@implementation BaseButton


- (void)initData{
    
    self.cornerRadius = 0.0f;
    self.borderWidth = 0.0f;
    self.titleNormalColor = AWTextColorGray();
    self.titleHighlightedColor = AWTextColorBlack();
    self.backgroundNormalColor = AWBGColorWhite();
    self.backgroundHighlightedColor =AWBGColorWhite();
    self.borderNormalColor = AWLineColorGray();
    self.borderHighlightedColor = AWLineColorGray();
}


- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initData];
        [self initLayout];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
        [self initLayout];
    }
    return self;
}

- (void)initLayout{
    [[self layer] setMasksToBounds:YES];
    
    // Corner Radius
    [self.layer setCornerRadius:self.cornerRadius];
    
    // Normal(Background) : 일반상태(배경)
    [self setBackgroundColor:self.backgroundNormalColor];
    
    // Normal(Title) : 일반상태(텍스트)
    [self setTitleColor:self.titleNormalColor forState:UIControlStateNormal];
    
    // Highlighted(Title) : 눌렸을때(텍스트)
    [self setTitleColor:self.titleHighlightedColor forState:UIControlStateHighlighted];
    
    // Border Line
    self.layer.borderWidth = self.borderWidth;
    self.layer.borderColor = self.borderNormalColor.CGColor;
    
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        [self setBackgroundColor:self.backgroundHighlightedColor];
        self.layer.borderColor = self.borderHighlightedColor.CGColor;
    } else {
        [self setBackgroundColor:self.backgroundNormalColor];
        self.layer.borderColor = self.borderNormalColor.CGColor;
    }
}

@end
