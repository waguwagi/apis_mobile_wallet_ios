//
//  BasePopupView.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, PopupAction) {
    PopupActionNext = 1,
    PopupActionCopy = 2,
    PopupActionCancel = 3,
    PopupActionResult = 4,
};

@protocol BasePopupViewDelegate;

@interface BasePopupView : BaseView
@property (assign, nonatomic) id<BasePopupViewDelegate> delegate;
@property (nonatomic) BasePopupModel * model;
@end

@protocol BasePopupViewDelegate <NSObject>
- (void)action:(BasePopupView * )view type:(PopupAction)action;

@end

NS_ASSUME_NONNULL_END
