//
//  AWButton.h
//  apismwallet
//
//  Created by Apis_iMac_001 on 03/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//
//    AWButton *button = [[AWButton alloc] initWithFrame:CGRectMake(50, 50, 100, 50)];
//    [button addTarget:self action:NSSelectorFromString(@"closePopup:") forControlEvents:UIControlEventTouchUpInside];
//    [button setTitle:[NSString stringWithFormat:@"%d", 2] forState:UIControlStateNormal];
//    [self.view addSubview:button];

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseButton : UIButton

@property float cornerRadius;
@property UIColor * titleNormalColor;
@property UIColor * titleHighlightedColor;
@property UIColor * backgroundNormalColor;
@property UIColor * backgroundHighlightedColor;

@property float borderWidth;
@property UIColor * borderNormalColor;
@property UIColor * borderHighlightedColor;

- (void)initData;
- (void)initLayout;

@end

NS_ASSUME_NONNULL_END
