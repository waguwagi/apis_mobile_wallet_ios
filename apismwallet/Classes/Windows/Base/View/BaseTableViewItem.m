//
//  BaseTableViewItem.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BaseTableViewItem.h"

@implementation BaseTableViewItem

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = AWBGColorBlueGray();
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
