//
//  BasePopupView.m
//  apismwallet
//
//  Created by Apis_iMac_001 on 17/01/2019.
//  Copyright © 2019 APIS. All rights reserved.
//

#import "BasePopupView.h"

@implementation BasePopupView
- (id)init{
    self = [super init];
    if(self){
    }
    
    NSLog(@"initinit");
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.model = [[BasePopupModel alloc]init];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
